// Print Hello World

//echo "Hello World";

// Comments

//Single line comment
#Also a single line comment
/*Double line comment*/

// Case Sensitivity

/*ECHO "No ";
EcHo "Case Sensitivity";*/

// Concatenation

/*$fname = "Sahil";
$lname = "Dhamija";
echo "My name is ".$fname." ".$lname;*/

// Var Scope

/*$x = 5;
function test() {
    x = 10;
    echo $x;
}
test(); // Error*/

/*$x = 5;
$y = 15;
function add() {
    global $x, $y;
    $sum = $x + $y;
    echo $sum;
}
add(); // Correct*/

/*$x = 5;
$y = 10;
function sum() {
    $GLOBALS['y'] = $GLOBALS['x'] + $GLOBALS['y'];
}
sum();
echo $y;*/

// Static Variable

/*function fun() {
    static $x = 0;
    echo $x;
    $x++;
}
fun();
fun();
fun();*/

// Diff b/w echo and print

/*echo "It", "can", "have", "multiple", "parameters", "<br />"; //no return value
print "It has only one parameter"; // return value 1*/

// Datatypes

/*$type_int = 5985;
$type_float = 10.365;
$type_bool = true;
$type_arr = array(1,2,3,4,5);
$type_string = "Sahil Dhamija";
echo $type_int."<br />";
echo $type_float."<br />";
echo $type_bool."<br />";
echo $type_arr[2]."<br />";
echo $type_string;*/

/*class Car {
    function Car() {
        $this->model = "BMW";
    }
}

$car1 = new Car();
echo $car1->model;*/

// String Functions

/*echo strlen("Sahil Dhamija")."<br />";
echo str_word_count("Mr. Sahil Dhamija")."<br />";
echo strrev("Sahil Dhamija")."<br />";
echo strpos("Sahil Dhamija", "Dhamija")."<br />";
echo str_replace("Dhamija", "Bansal", "Sahil Dhamija");*/

// Number Functions

/*$num = 51;
echo is_int($num);
$num1 = 13.21;
echo is_float($num1);
$num2 = "59";
echo is_numeric($num2);*/

// Type Casting

/*$x = 23465.768;
$int_cast = (int)$x;
echo $int_cast."<br />";*/

/*$x = "23465.768";
$int_cast = (int)$x;
echo $int_cast + 26;*/

// Constant

/*define("MyName", "Sahil Dhamija");
echo MyName;*/

/*define("prog_lang", ["PHP", "JavaScript", "Python"]);
echo prog_lang[0];*/

// Arithmetic Operators

/*$x = 5;
$y = 2;
echo $x + $y."<br />";
echo $x - $y."<br />";
echo $x * $y."<br />";
echo $x / $y."<br />";
echo $x % $y."<br />";*/

// Increment, Decrement & Compound Assignment

/*$a = 12;
echo $a++;
echo ++$a;
echo $a--;
echo --$a;
echo $a+=2;
echo $a*=2;*/

// Conditional/Ternary Operator

/*$n1 = 7;
$n2 = 18;
$big = $n1 > $n2 ? $n1 : $n2;
echo $big;*/

// If-Else

/*$age = 28;
if($age < 18)
    echo "Below 18";
else if($age == 18)
    echo "Equals 18";
else
    echo "Above 18";*/

// Logical Operators

/*$a = 95; $b = 40; $c = 15;
if($a > $b && $a > $c)
    echo $a;
else if($b > $a && $b > $c)
    echo $b;
else
    echo $c;*/

// Switch Statement

/*$a = 46;
$b = 18;
$c = 3;
switch($c) {
    case 1: echo $a + $b;
    break;
    case 2: echo $a - $b;
    break;
    case 3: echo $a * $b;
    break;
    case 4: echo $a / $b;
    break;
    default: echo "Invalid Calculation Type";
}*/

// Loops

/*$i = 1;
while($i <= 10) {
    echo $i."<br />";
    $i++;
}*/

/*$i = 1;
do {
    echo $i."<br />";
    $i++;
} while($i <= 10);*/

/*for($i = 2; $i <= 20; $i += 2)
    echo $i."<br />";*/

/*$nums = array(1,2,3,4,5);
foreach($nums as $i) {
    echo $i."<br />";
}*/

// Functions

/*function dispMsg() {
    echo "Hello World";
}
dispMsg();*/

/*function add($x, $y) {
    echo $x + $y;
}
add(2.77,3);*/

/*function addNumbers(int $a, int $b) {
    return $a + $b;
}
echo addNumbers(5, "5"); //Error*/

/*function sub($a, $b=20) {
    return $a - $b;
}
echo sub(50);*/

// Arrays

/*$numbers = array(2,4,6,8,10,12,14);
echo count($numbers);*/

/*$cars = array("Audi", "BMW", "Ferrari");
$arr_len = count($cars);
for($i = 0; $i < $arr_len; $i++) {
    echo $cars[$i];
    echo "<br />";
}*/

/*$age = array("Father"=>"62", "Mother"=>"53", "Me"=>"22");
echo "My Father is ". $age['Father']. " years old!";*/

/*$cars = array
(
array("Audi", 22, 18),
array("BMW", 15, 13),
array("Ferrari", 5, 2),
array("Land Rover", 17, 15)
);

echo "Model: ".$cars[2][0]."| In Stock: ". $cars[2][1]. "| Cars Sold: ". $cars[2][2];*/

// $GLOBALS

/*$x = 7;
$y = 2;
function add() {
    $GLOBALS['z'] = $GLOBALS['x'] + $GLOBALS['y'];
}
add();
echo $z;*/

// $_SERVER

/*echo $_SERVER['SERVER_NAME']."<br />";
echo $_SERVER['HTTP_HOST']."<br />";
echo $_SERVER['SCRIPT_NAME']."<br />";*/