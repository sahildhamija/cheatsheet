// console.log("Hello World");
// console.error("This is an error");
// console.warn("This is a warning");

// var, const, let
/*const age = 22;
console.log(age);

let a, b;
a = 5, b = 10;
console.log(a+b);*/

// DataTypes
/*let name = 'Sahil'; //String
let age = 22; // Integer
let height = 5.7; // Float
let isMale = true; // Boolean
let bad_habbits = null; // null
let salary; // undefined

console.log(typeof bad_habbits);*/

// Concatenation
/*const fname = 'Sahil';
const lname = 'Dhamija';
console.log(fname + " " + lname);
// Template String
console.log(`My name is ${fname} ${lname}`);*/

// String Properties & Methods
/*var str = 'Hello';
console.log(str.length);
console.log(str.toUpperCase());
console.log(str.toLowerCase());
console.log(str.substring(0,4));
console.log(str.split(''));*/

// Arrays
/*const numbers = new Array(1,2,3,4,5);
console.log(numbers);

let string = ['sahil', 'shivam', 'rajat'];
string.pop(); // remove last one
console.log(string);

var mul_types = ['apple', 'grapes', 10, 3.14159, true];
mul_types.push('last');
mul_types.unshift('first');
console.log(mul_types.indexOf(3.14159));
console.log(Array.isArray(mul_types));
console.log(mul_types);
console.log(mul_types[3]);*/

// Objects
/*const person {
  firstName: 'Sahil',
  lastName: 'Dhamija',
  age: 22,
  hobbies: ['coding', 'sports', 'songs'],
  address: {
    area: 'Uttam Nagar',
    city: 'New Delhi',
    pincode: 110059
  }
}

person.email: 'sdhamija1997@gmail.com';

console.log(person);
console.log(person.firstName, person.lastName);
console.log(person.hobbies[1]);
console.log(person.address.city);

const { firstName, lastName, address: { city }} = person;
console.log(city);*/

// For Loop
/*for(let i = 1; i <= 10; i++)
  console.log(`For Loop Number: ${i}`);*/

// While Loop
/*let i = 1;
while(i <= 10)
  {
    console.log(`While Loop Number: ${i}`);
    i++;
  }*/

// For each Loop
/*let x = [1,2,3,4,5];
for(let i of x)
  console.log(`For each loop number: ${i}`);*/

// If Else
/*let age = prompt("Enter your age ?");
if(age < 18)
  console.log("Below 18");
else if(age === 18)
  console.log("Equal to 18");
else
  console.log("Above 18");*/

// Conditional Operator
/*let a = 40, b = 15;
let big = (a > b) ? a : b;
console.log(big);*/

// Switch Statement
/*let n1 = +prompt("Enter first number: ");
let n2 = +prompt("Enter second number: ");
let cal = prompt("Calculation type: ");
switch(cal) {
  case '+': console.log(n1 + n2);
    break;
  case '-': console.log(n1 - n2);
    break;
  case '*': console.log(n1 * n2);
    break;
  case '/': console.log(n1 / n2);
    break;
  default: console.error("Invalid calculation type");
}*/

// Function
/*function addTwoNumbers(n1, n2) {
  return n1 + n2;
}

console.log(addTwoNumbers(2,3));*/

// Arrow Function
/*const addTwo = (n1, n2) => {
  return n1 + n2;
}

console.log(addTwo(5,5));*/

// Constructor Function
/*function Person(firstName, lastName, DOB) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.DOB = new Date(DOB);
}

// Instantiate Object
const person1 = new Person('Sahil', 'Dhamija', '04-11-1997');
const person2 = new Person('Shivam', 'Gomat', '02-07-1998');

console.log(person1);
console.log(person2.DOB);*/

// Class
/*class Person {
  constructor(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
  }
  getBirthYear() {
    return this.dob.getFullYear();
  }
  getFullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}

// Instantiate Object
const person1 = new Person('Sahil', 'Dhamija', '04-11-1997');
console.log(person1);*/

/* Examine the Document Objects */
    //console.dir(document);
    //console.log(document.domain);
    //console.log(document.URL);
    //console.log(document.title);
    //document.title = "Sahil Dhamija";
    //console.log(document.doctype);
    //console.log(document.head);
    //console.log(document.body);
    //console.log(document.all);
    //console.log(document.all[10]);
    //document.all[10].textContent = "Item List";
    //console.log(document.forms);
    //console.log(document.links);
    //console.log(document.images);

    /* Get Element By ID */
    //var headerTitle = document.getElementById('header-title');
    //console.log(headerTitle);
    //headerTitle.textContent = 'Item List'; // Not affected by CSS
    //headerTitle.innerText = 'Listed Items'; // Affected by CSS
    //console.log(headerTitle.innerHTML);
    //headerTitle.innerHTML = '<h1>Item List</h1>';
    //var header = document.getElementById('main-header');
    //header.style.border = 'solid 5px #000';

    /* Get Elements By Class Name */
    //var items = document.getElementsByClassName('list-group-item');
    //console.log(items);
    //console.log(items[1]);
    //items[1].textContent = 'Item Name Changed';
    //items[0].style.fontWeight = 'bold';
    //items[3].style.backgroundColor = 'yellow';
    /*for(var i = 0; i < items.length; i++)
      items[i].style.backgroundColor = 'red';*/

    /* Get Elements By Tag Name */
    //var li = document.getElementsByTagName('li');
    //console.log(li);
    //console.log(li[1]);
    //li[1].textContent = 'Item Name Changed';
    //li[0].style.fontWeight = 'bold';
    //li[3].style.backgroundColor = 'yellow';
    /*for(var i = 0; i < li.length; i++)
      li[i].style.backgroundColor = 'red';*/

    /* Query Selector */
    //var header = document.querySelector('#main-header');
    //header.style.border = 'solid 5px #ccc';
    //var input = document.querySelector('input');
    //input.value = 'Item 5'; // by default select first
    //var submit = document.querySelector('input[type="submit"]');
    //submit.value = 'ADD';
    //var item = document.querySelector('.list-group-item');
    //item.style.color = 'red'; // by default select first
    //var lastItem = document.querySelector('.list-group-item:last-child');
    //lastItem.style.color = 'blue';
    //var secondItem = document.querySelector('.list-group-item:nth-child(2)');
    //secondItem.style.color = 'green';

    /* Query Selector All */
    //var titles = document.querySelectorAll('.title');
    //console.log(titles);
    //titles[0].textContent = 'Add Item';
    //var odd = document.querySelectorAll('li:nth-child(odd)');
    //var even = document.querySelectorAll('li:nth-child(even)');
    /*for(var i = 0; i < odd.length; i++) {
        odd[i].style.backgroundColor = '#f4f4f4';
        even[i].style.backgroundColor = '#ccc';
    }*/

    /* Traversing DOM */
    //var itemList = document.querySelector('#items');
    //parentNode
    /*console.log(itemList.parentNode);
    itemList.parentNode.style.backgroundColor = '#f4f4f4';
    console.log(itemList.parentNode.parentNode.parentNode);*/
    //parentElement
    /*console.log(itemList.parentElement);
    itemList.parentElement.style.backgroundColor = '#f4f4f4';
    console.log(itemList.parentElement.parentElement.parentElement);*/
    //childNodes
    /*console.log(itemList.childNodes); // Including Line Breaks
    console.log(itemList.childrens);
    console.log(itemList.childrens[1]);
    itemList.children[1].style.backgroundColor = 'yellow';*/
    //firstChild
    //console.log(itemList.firstChild); // including blank space and line breaks
    //firstElementChild
    //console.log(itemList.firstElementChild);
    //lastChild
    //console.log(itemList.lastChild);
    //lastElementChild
    //console.log(itemList.lastElementChild);
    //nextSibling
    //console.log(itemList.nextSibling);
    //nextElementSibling
    //console.log(itemList.nextElementSibling);
    //previousSibling
    //console.log(itemList.previousSibling);
    //previousElementSibling
    //console.log(itemList.previousElementSibling);

    /* Create Element and Text Node */
    //var newDiv = document.createElement('div');
    //newDiv.className = 'class1';
    //newDiv.id = 'id1';
    //newDiv.setAttribute('title', 'val1');
    //var newDivText = document.createTextNode('Hello World');
    //newDiv.appendChild(newDivText);
    //var container = document.querySelector('header .container');
    //var h1 = document.querySelector('header h1');
    //console.log(newDiv);
    //newDiv.style.fontSize = '30px';
    //container.insertBefore(newDiv, h1);

// Object Literal
/*const book = {
  title: 'JavaScript OOP',
  author: 'Sahil Dhamija',
  year: 2019,
  getSummary: function() {
    return `${this.title} was written by ${this.author} in ${this.year}`;
  }
};
console.log(book);
console.log(book.getSummary());
console.log(Object.values(book));
console.log(Object.keys(book));*/

// Constructor
/*function Book(title, author, year) {
  this.title = title;
  this.author = author;
  this.year = year;
  this.getSummary = function() {
    return `${this.title} was written by ${this.author} in ${this.year}`;
}

const book1 = new Book('JavaScript', 'Sahil Dhamija', 2019);
console.log(book1.getSummary());*/

//Prototype
/*function Book(title, author, year) {
  this.title = title;
  this.author = author;
  this.year = year;
}

Book.prototype.getSummary = function() {
    return `${this.title} was written by ${this.author} in ${this.year}`};

const book1 = new Book('JavaScript', 'Sahil Dhamija', 2019);
console.log(book1.getSummary());*/

//Inheritance
function Book(title, author, year) {
  this.title = title;
  this.author = author;
  this.year = year;
}

Book.prototype.getSummary = function() {
    return `${this.title} was written by ${this.author} in ${this.year}`};

function Magazine(title, author, year, month) {
  Book.call(this, title, author, year);
  this.month = month;
}

Magazine.prototype = Object.create(Book.prototype);

const magz = new Magazine('JavaScript', 'Sahil Dhamija', 2019, 'Nov');
console.log(magz.getSummary());